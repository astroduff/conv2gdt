# OPT += DHAVE_HDF5
#--------------------------------------- Select target Computer

SYSTYPE="epic"

CC       = ifort
OPTIMIZE =  # optimization and warning flags (default)
OPT = -fPIC #-O1 -g -traceback -check all -fp-stack-check

ifeq ($(SYSTYPE),"epic")
HDF5INCL  = -I/ivec/devel/intel/12.1/hdf5/1.8.7/include
HDF5LIB   = -L/ivec/devel/intel/12.1/hdf5/1.8.7/lib -lhdf5 -Wl,--rpath -Wl,/ivec/devel/intel/12.1/hdf5/1.8.7/lib
endif
#HDF5WRAPINCL = -I/home/aduffy/code/hdf5_wrapper/src/../lib 
#HDF5WRAPLIB =  -L/home/aduffy/code/hdf5_wrapper/src/../lib -Wl,-rpath=/home/aduffy/code/hdf5_wrapper/src/../lib 

OPTIONS = $(OPTIMIZE) $(OPT)
LINKFLAGS = -I/ivec/devel/intel/12.1/hdf5/1.8.7/lib -I/ivec/devel/intel/12.1/hdf5/1.8.7/include/ -I/home/aduffy/code/hdf5_wrapper/src/../lib -L/home/aduffy/code/hdf5_wrapper/src/../lib -Wl,-rpath=/home/aduffy/code/hdf5_wrapper/src/../lib -I/home/aduffy/code/hdf5_wrapper/src/../lib -lhdfwrapper

EXEC   = lgrfcgdt_hdf5

FILES  = grafic2gadget.f90 time.f

OBJS   = grafic2gadget.o time.o

LIBS = $(HDF5LIB) -lhdf5_fortran 

INCL = $(HDF5INCL)

# The executable needs the objects (first line) then the exe is created with the complier in CC 
# acting with options on the objects and libraries 
$(EXEC): $(OBJS)
	$(CC) $(OPTIONS) $(LINKFLAGS) -o $(EXEC) $(OBJS)

# To create the objects for the executable we need to run the compiler on the fortran files
$(OBJS): $(FILES)
	 $(CC) $(OPTIONS) $(LINKFLAGS) -c $(FILES)

# Assuming you want to clean only delete the objects and executable
clean:
	rm -f $(OBJS) $(EXEC)
