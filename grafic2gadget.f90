program lgrfc2gdt_hdf5
!!  USE HDF5 ! This module contains all necessary modules
  USE hdf5_wrapper

  implicit none
!! HDF5 Data

    CHARACTER(kind=1, len=80) :: dsetname, attname, partgrp     ! Dataset name
    INTEGER :: file_id       ! File identifier

!! GRAFIC data
  integer(kind=4) :: np1,np2,np3
  real(kind=4) :: dx,x1o,x2o,x3o
  real(kind=4) :: astart,omegam,omegav,h0

  real(kind=4) :: dx0

  real(kind=4), allocatable :: dummy(:,:,:)
  real(kind=4), allocatable :: pos(:,:),vel(:,:)
  real(kind=4), allocatable :: deltab(:,:,:)
  real(kind=8), allocatable :: f(:,:,:)
  real(kind=8) :: OrigBoxSize
  ! AD what happened to the deltab array?

  real(kind=4) :: vfact,ivfact,offset,x0,y0,z0
  real(kind=4) :: xmin,xmax,ymin,ymax,zmin,zmax,x,y,z
  real(kind=4), allocatable :: lbnd(:,:),ubnd(:,:)
  integer(kind=8), allocatable :: larr(:)
  integer(kind=1), allocatable :: keeparr(:)

  real(kind=4), external :: fomega,dladt

  real(kind=4), parameter :: utherm=10.0*10.0
  real(kind=4), allocatable :: uthermarr(:)
  real(kind=4) :: fbar

  real(kind=4), parameter :: rhocrit=27.755

  !!! GADGET data 
  integer(kind=4), dimension(6) :: np,nall,resarr
  real(kind=8), dimension(6) :: massarr
  real(kind=8) :: expansion, redshift
  integer(kind=4) :: flag
  integer(kind=4), dimension(6) :: highword
  integer(kind=4) :: NumFiles
  real(kind=8) :: BoxSize,Omega0,OmegaLambda,HubbleParam         
  !! Count by number of a given size (either 4 or 8) should leave 64 char to fill to 256
  !! np -> 6*4
  !! massarr -> 6*8
  !! expansion -> 1*8
  !! redshift -> 1*8
  !! flag_sfr -> 1*4
  !! flag_feedback -> 1*4
  !! npTotal (nall) -> 6*4
  !! flag_cooling -> 1*4
  !! numfiles -> 1*4
  !! then BoxSize, Omega0, OmegaL, HubbleParam each 8 -> 4*8 
  !! flag_stellarage -> 1*4
  !! flag_metals -> 1*4
  !! npTotalHighword -> 6*4
  !! flag_entropy instead of thermal energy -> 1*4 isn't specified (why not Chris?) 
  character(kind=1,len=256-6*4-6*8-2*8-2*4-6*4-2*4-4*8-2*4-6*4) :: unused
  real(kind=8) :: dvar
  !!

  integer(kind=4), dimension(6) :: np_offset
  integer(kind=4) :: ncount,ngas,res,ngas_orig
  integer(kind=8) :: npart,i,j,npart_orig,l
  integer(kind=4) :: i_s
  integer(kind=4) :: ip,im,ip2,im2,ip3,im3,ip4,im4
  integer(kind=8) :: k,m,n,lnew
  integer(kind=8) :: minlnew,maxlnew
  integer(kind=4) :: i1,i2,i3,j1,j2,j3
  integer(kind=4), allocatable :: nump_level(:),rescube(:),resorig(:)
  real(kind=4), allocatable :: mpart_level(:)
  integer(kind=4), allocatable :: shortid(:)
  integer(kind=8), allocatable :: longid(:)
  logical :: fexist

  integer(kind=8) :: indx_offset, start_indx, end_indx
  integer(kind=4) :: phase,subfile
  integer(kind=4) :: nlevel,nlevel_max,AllocateStatus,ntype,nloop

  character(kind=1,len=132) :: infile,outfile,fullfile
  character(kind=1,len=4) :: char_nfile
  character(kind=1,len=10) :: lev_n
  character(kind=1,len=20), dimension(7) :: input
  character(kind=1,len=20) :: instring
  
  logical :: isdebug

  integer(kind=4) :: in,len,istat,strfrom,lenstr
  logical :: isdmonly,isecho,usehdf

  ! Process command line arguments

  if(command_argument_count().lt.1) &
       & stop 'Usage: grafic2gadget -nlev <nlevel_max> -out <filename> -nfile <NumFiles> -fbar <fbar> (-dm_only) -res <res> (-use_hdf5)'

  in=0
  usehdf=.false.
  isdmonly=.false.
  isdebug=.false.
  fbar=0.0
  NumFiles = 1
  ! Remove every 'res' particle from sim. So if res=2 then every second particle IN EACH DIRECTION is removed 
  ! res=2 would decrease particle number by 8, increase mass of particle by 8 and increase dx by 8
  res=1

  do
     in=in+1
     call get_command_argument(in,instring,len,istat)

     if(istat.gt.0) exit

     if(instring.eq.'-nlev') then
        in=in+1
        call get_command_argument(in,instring,len,istat)
        read(instring,*) nlevel_max
        write(*,*) 'nlevel_max is ', nlevel_max
        cycle
     else if(instring.eq.'-out') then
        in=in+1
        call get_command_argument(in,outfile,len,istat)
        write(*,*) 'Out file is ', outfile
        write(*,*) 
        cycle
     else if(instring.eq.'-fbar') then
        in=in+1
        call get_command_argument(in,instring,len,istat)
        read(instring,*) fbar
        write(*,*) 'fbar is ',fbar
        write(*,*)
        cycle
    else if(instring.eq.'-nfile') then
        in=in+1
        call get_command_argument(in,instring,len,istat)
        read(instring,*) NumFiles
        write(*,*) 'Number subfiles is ',NumFiles
        write(*,*)
        cycle
    else if(instring.eq.'-dm_only') then
        isdmonly=.true.
        write(*,*) 'DM ONLY'
        write(*,*)
        cycle
     else if(instring.eq.'-res') then
        in=in+1
        call get_command_argument(in,instring,len,istat)
        read(instring,*) res
        write(*,*) 'res is ', res
        write(*,*)
        cycle
     else if(instring.eq.'-use_hdf5') then
        usehdf=.true.
        write(*,*) 'HDF5 output selected'
        write(*,*)
        cycle
     end if
  end do

  ! Remove the hdf5 or bin appended file types
  if(usehdf.eqv..true.) then
    strfrom = INDEX(outfile, '.hdf5', BACK = .TRUE.)
  else
    strfrom = INDEX(outfile, '.bin', BACK = .TRUE.)
  end if
  lenstr = len_trim(outfile)
  if (strfrom.eq.0) then
    strfrom = lenstr
  end if

  if(isdmonly.eqv..false.) then
     if(fbar.le.0.0) stop 'Need to define fbar...'
  end if

  ! Quickly check that files exist...

  do nlevel=nlevel_max,0,-1
     write(lev_n,'(a5,i1,a1)') 'level',nlevel,'/'

     infile=trim(lev_n)//'ic_deltab'
     
     inquire(file=infile,exist=fexist)

     if(fexist.eqv..false.) then
        write(*,'(a28,i3)') 'Cannot find files for level ',nlevel
        stop
     end if
  end do

  allocate(nump_level(1+nlevel_max), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory for nump_level ***"
  allocate(lbnd(3,1+nlevel_max),ubnd(3,1+nlevel_max),mpart_level(1+nlevel_max), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory for mpart_level ***"

  allocate(resorig(1+nlevel_max), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory for resorig ***"
  allocate(rescube(1+nlevel_max), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory for rescube ***"

  !! Set up particle array to calculate the increase in particle mass
  do n=1,1+nlevel_max
     rescube(n)=res**3
     resorig(n)=res
  end do
  if(nlevel_max.ge.1) then 
     !! Reset the highest res
        rescube(1+nlevel_max)=1
        resorig(1+nlevel_max)=1
  end if

  write(*,'(a42)') '=========================================='
  write(*,'(a42)') 'Converting from GRAFIC to GADGET format...'
  write(*,'(a42)') '=========================================='
  write(*,*)

  if(isdmonly) write(*,'(a28)') 'Assuming dark matter only...'

! Check for numbers of particles

  npart=0
  ngas=0
  
  xmin=1.e9
  ymin=1.e9
  zmin=1.e9
  xmax=-1.e9
  ymax=-1.e9
  zmax=-1.e9

  do nlevel=nlevel_max,0,-1
     lbnd(1,1+nlevel)=xmin
     ubnd(1,1+nlevel)=xmax

     lbnd(2,1+nlevel)=ymin
     ubnd(2,1+nlevel)=ymax

     lbnd(3,1+nlevel)=zmin
     ubnd(3,1+nlevel)=zmax
     
     write(lev_n,'(a5,i1,a1)') 'level',nlevel,'/'
     write(*,'(a18,a)') 'Reading data from ',trim(lev_n)

     infile=trim(lev_n)//'ic_deltab'

     open(11,file=infile,form='unformatted')
     read(11) np1,np2,np3,dx,x1o,x2o,x3o,astart,omegam,omegav,h0
     write(*,*) np1,np2,np3,dx,x1o,x2o,x3o,astart,omegam,omegav,h0
     close(11)

     npart=npart+np1*np2*np3

     if(isdmonly.eqv..false.) then
        if(nlevel.eq.nlevel_max) then
           ngas=np1*np2*np3
           npart=npart+ngas
        end if
     end if

     if(nlevel.eq.nlevel_max) then
        expansion=astart
        redshift=1./astart-1.
        write(*,'(a18,2f12.6)') 'Initial aexp, z : ',expansion,redshift
        Omega0=omegam
        OmegaLambda=omegav
        HubbleParam=0.01*h0
        write(*,'(a7,f10.6,a14,f10.6,a3,f10.6)') 'Omega0:',Omega0,', OmegaLambda:',OmegaLambda,', h:',HubbleParam
     end if

     if(nlevel.eq.0) then
        dx0=dx
        OrigBoxSize=dx0*np1
        BoxSize=dx0*np1*HubbleParam
     end if

     nump_level(1+nlevel)=0
     mpart_level(1+nlevel)=rhocrit*Omega0*((dx*np1*0.01*h0)**3)/real(np1*np2*np3)
     
     xmin=x1o
     ymin=x2o
     zmin=x3o
     
     xmax=x1o+np1*dx
     ymax=x2o+np2*dx
     zmax=x3o+np3*dx

  write(*,*) 'xmin and xmax are ',xmin,xmax
  write(*,*) 'ymin and ymax are ',ymin,ymax
  write(*,*) 'zmin and zmax are ',zmin,zmax

  end do
! End of nlevel loop setting up the boxsize and particle mass arrays etc  
  write(*,*)
  write(*,'(a29,i10)') 'Maximum number of particles: ',npart
  write(*,*)


  if(nlevel_max.gt.0) then 
! Allocate keeparr for multiresolution runs... won't work, need to allocate it for all particle species
  if(allocated(keeparr).eqv..false.) allocate(keeparr(npart), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory for keeparr ***"
  end if

! Loop over position first, output to file, and then loop velocity
    do phase=0,1
! phase == 0 means position
! phase == 1 means velocity
       if (fbar.gt.0) then
          if (phase.eq.1) isdmonly=.false. !! Reset the dmonly switch
          if (phase.eq.0) ngas_orig=ngas
          if (phase.eq.1) ngas=ngas_orig
       end if
       if (phase.eq.0) npart_orig=npart
       if (phase.eq.1) npart=npart_orig

  ! Allocate variables...
  if (phase.eq.0) then
  if(allocated(pos).eqv..false.) allocate(pos(3,npart), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory for pos ***"
  end if
  if (phase.eq.1) then
  if(allocated(vel).eqv..false.) allocate(vel(3,npart), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory for vel ***"
  end if
  ! Now loop over levels, starting at nlevel_max and working down to 0.
  ncount=0   ! Particle counter
 
  do nlevel=nlevel_max,0,-1
     
     nump_level(1+nlevel)=0

     write(lev_n,'(a5,i1,a1)') 'level',nlevel,'/'
     
     input(1)=trim(lev_n)//'ic_deltab'
     input(2)=trim(lev_n)//'ic_velbx'
     input(3)=trim(lev_n)//'ic_velby'
     input(4)=trim(lev_n)//'ic_velbz'
     input(5)=trim(lev_n)//'ic_velcx'
     input(6)=trim(lev_n)//'ic_velcy'
     input(7)=trim(lev_n)//'ic_velcz'
     
     n=1

     if(isdmonly.eqv..true.) n=5
     
     do
        if(n.gt.7) exit
        
        infile=input(n)

        inquire(file=infile,exist=fexist)
        
        if(fexist.eqv..false.) cycle
        
        open(11,file=infile,form='unformatted')
        !!rewind 11
        read(11) np1,np2,np3,dx,x1o,x2o,x3o,astart,omegam,omegav,h0

        if(allocated(dummy).eqv..false.) allocate(dummy(np1,np2,np3), STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "*** Not enough memory for dummy ***"
        !  velocity (proper km/s) =  Displacement (comoving Mpc at astart) * 
        !  vfact, where vfact = dln(D+)/dtau with tau=conformal time.
        !  Functions fomega and dladt taken from time.f in grafic.

        vfact=fomega(astart,omegam,omegav) &
             &       *h0*dladt(astart,omegam,omegav)/astart

        ivfact = 1./vfact
        offset = 0.5*(dx0-dx)

        if(nlevel.eq.nlevel_max.and.n.eq.5) then
           write(*,'(a15,f10.6)') 'dln(D+)/dtau : ',vfact
           write(*,*)'H0 is :',h0
           write(*,*) 'Value of offset :',offset,', Level:',nlevel,dx0/dx
           flush(6)
        end if

        indx_offset=np1*np2*np3
        if(isdmonly.eqv..true.) indx_offset=0
        
        do i3=1,np3
           read(11) ((dummy(i1,i2,i3),i1=1,np1),i2=1,np2)
           z0=x3o+(i3-1)*dx+offset

           do i2=1,np2
              y0=x2o+(i2-1)*dx+offset
              do i1=1,np1
                 x0=x1o+(i1-1)*dx+offset

                 l=i1+(i2-1)*np1+(i3-1)*np2*np1
   
                 if(n.eq.2.or.n.eq.5) then
                    x=x0+dummy(i1,i2,i3)*ivfact
                    if(n.eq.5) l=l+indx_offset
                    if (phase.eq.0) pos(1,l+ncount)=(x0+dummy(i1,i2,i3)*ivfact)
                    if (phase.eq.1) vel(1,l+ncount)=dummy(i1,i2,i3)/sqrt(astart)
                    if(n.eq.5) nump_level(1+nlevel)=nump_level(1+nlevel)+1                    
                 else if(n.eq.3.or.n.eq.6) then
                    y=y0+dummy(i1,i2,i3)*ivfact
                    if(n.eq.6) l=l+indx_offset
                    if (phase.eq.0) pos(2,l+ncount)=(y0+dummy(i1,i2,i3)*ivfact)
                    if (phase.eq.1) vel(2,l+ncount)=dummy(i1,i2,i3)/sqrt(astart)
                 else if(n.eq.4.or.n.eq.7) then
                    z=z0+dummy(i1,i2,i3)*ivfact
                    if(n.eq.7) l=l+indx_offset
                    if (phase.eq.0) pos(3,l+ncount)=(z0+dummy(i1,i2,i3)*ivfact)
                    if (phase.eq.1) vel(3,l+ncount)=dummy(i1,i2,i3)/sqrt(astart)
                 end if
              end do !! End do x (or i1)
           end do !! End do y (or i2)
        end do !! End do z (or i3)
        
        close(11)

        deallocate(dummy, STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "*** Couldn't deallocate dummy ***"

        if(isdebug.eqv..true.) write(*,*) 'Finished reading ',trim(infile)
        n=n+1
        end do !! End do n, still no idea what deltab does!

        ntype=0
        if(isdmonly.eqv..false.) then
           if(nlevel.eq.nlevel_max) then
              ntype=1 !! Will extend loop from DM to now Gas + DM
           end if
        end if
           
        !! Still looping over nlevel
        if(resorig(1+nlevel).gt.1) then
           write(*,*)'deRefining by ',resorig(1+nlevel)

        ! AD Got to do the derefinement here
        ! secret is the array selecting the cube with res particles each side
        nump_level(1+nlevel) = nump_level(1+nlevel)/rescube(1+nlevel)
        if (ntype.eq.1) ngas = ngas/rescube(1+nlevel)

        allocate(larr(rescube(1+nlevel)), STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "*** Not enough memory for larr ***"
        write(*,*)'On level ',nlevel,' with nump_level array ', nump_level(:)

        do nloop=0,ntype
        do i3=1,np3/resorig(1+nlevel)
           do i2=1,np2/resorig(1+nlevel)
              do i1=1,np1/resorig(1+nlevel)
                 lnew=i1
                 lnew=lnew+(i2-1)*(np1/resorig(1+nlevel))
                 lnew=lnew+(i3-1)*(np2/resorig(1+nlevel))*(np1/resorig(1+nlevel))
                 if(nloop.eq.1) lnew=lnew+ngas !! After gas add the indx_offset for DM

                 !! Construct the subcube to average over
                 i=1
                 do j3=1,resorig(1+nlevel)
                    do j2=1,resorig(1+nlevel)
                       do j1=1,resorig(1+nlevel)
                          larr(i)=j1+(i1-1)*resorig(1+nlevel)
                          larr(i)=larr(i)+(j2+(i2-1)*resorig(1+nlevel)-1)*np1
                          larr(i)=larr(i)+(j3+(i3-1)*resorig(1+nlevel)-1)*np1*np2
                          if(nloop.eq.1) larr(i)=larr(i)+indx_offset !! After gas add the indx_offset for DM
                          i=i+1
                       end do
                    end do
                 end do

                ! Centre of Mass position and Velocity (assume equal weight particles)
                 do k=1,3
                    if (phase.eq.0) then
                       pos(k,lnew+ncount)=sum(pos(k,larr))/(1.*rescube(1+nlevel))
                    end if
                    if (phase.eq.1) then
                       vel(k,lnew+ncount)=sum(vel(k,larr))/(1.*rescube(1+nlevel))
                    end if
                 end do

              end do !! End of loop in x
           end do !! End of loop in y
        end do !! End of loop in z        

        end do !! End of nloop for gas sim

        deallocate(larr, STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "*** Couldn't deallocate larr ***"
     end if !! End of deRefinement case
     flush(6)

     if (phase.eq.0) then
     ! Perform a loop to ensure all particles are obeying the periodic boundary conditions
     do i=1+ncount,nump_level(1+nlevel)+ncount
        do k=1,3 
           if(pos(k,i).lt.0) pos(k,i)=pos(k,i)+OrigBoxSize
           if(pos(k,i).ge.OrigBoxSize) pos(k,i)=pos(k,i)-OrigBoxSize
           if(isdmonly.eqv..false.) then
              lnew = i + ngas !! After gas add the indx_offset for DM
              if(pos(k,lnew).lt.0) pos(k,lnew)=pos(k,lnew)+OrigBoxSize
              if(pos(k,lnew).ge.OrigBoxSize) pos(k,lnew)=pos(k,lnew)-OrigBoxSize
           end if
        end do
     end do
     end if
     
     if(nlevel.ne.nlevel_max) then
        j=ncount
        ! Remove overlapping low res particles in refined regions
        do i=1+ncount,nump_level(1+nlevel)+ncount
           if(phase.eq.0) then
           ! Only do this loop for phase == 0, i.e. with positions
           x=floor((pos(1,i)-lbnd(1,1+nlevel))/(ubnd(1,1+nlevel)-lbnd(1,1+nlevel)))
           y=floor((pos(2,i)-lbnd(2,1+nlevel))/(ubnd(2,1+nlevel)-lbnd(2,1+nlevel)))
           z=floor((pos(3,i)-lbnd(3,1+nlevel))/(ubnd(3,1+nlevel)-lbnd(3,1+nlevel)))
           keeparr(i) = 0!! Magic count tracks which particles you keep, one if kept, zero to start
           if(x.eq.0.0.and.y.eq.0.0.and.z.eq.0.0) cycle
           keeparr(i) = 1!! Magic count tracks which particles you keep
           end if !! end of phase == 0

           if(keeparr(i).eq.1) then
           j=j+1

           do k=1,3
            if(phase.eq.0) pos(k,j)=pos(k,i)*HubbleParam
            if(phase.eq.1) vel(k,j)=vel(k,i)
           end do !! End of loop over x, y, z

           end if !! if particle is kept, i.e. keeparr eq 1

        end do !! End of low res particles
        nump_level(1+nlevel)=j-ncount
     else
! AD multiply the highest res region by Hubble Parameter too
        do i=1,nump_level(1+nlevel)
           do k=1,3
            if(phase.eq.0) then
               pos(k,i)=pos(k,i)*HubbleParam
            ! AD double count for the case of gas to force DM count
               if(isdmonly.eqv..false.) then
                  lnew = i + ngas !! After gas add the indx_offset for DM
                  pos(k,lnew)=pos(k,lnew)*HubbleParam
               end if
            end if
         end do
      end do
! AD end of my addition
     end if

     if(phase.eq.0) then
     ! AD Check that the values are scaled to 'h'
     xmin=1.e9
     ymin=1.e9
     zmin=1.e9
     xmax=-1.e9
     ymax=-1.e9
     zmax=-1.e9
     
     do i=1+ncount,ncount+nump_level(1+nlevel)
        if(isdmonly.eqv..false.) then
        if(xmin.gt.pos(1,i+ngas)) xmin=pos(1,i+ngas)
        if(xmax.lt.pos(1,i+ngas)) xmax=pos(1,i+ngas)
        if(ymin.gt.pos(2,i+ngas)) ymin=pos(2,i+ngas)
        if(ymax.lt.pos(2,i+ngas)) ymax=pos(2,i+ngas)
        if(zmin.gt.pos(3,i+ngas)) zmin=pos(3,i+ngas)
        if(zmax.lt.pos(3,i+ngas)) zmax=pos(3,i+ngas)
        endif
        if(xmin.gt.pos(1,i)) xmin=pos(1,i)
        if(xmax.lt.pos(1,i)) xmax=pos(1,i)
        if(ymin.gt.pos(2,i)) ymin=pos(2,i)
        if(ymax.lt.pos(2,i)) ymax=pos(2,i)
        if(zmin.gt.pos(3,i)) zmin=pos(3,i)
        if(zmax.lt.pos(3,i)) zmax=pos(3,i)
     end do
     write(*,*) 'nlevel is ',nlevel
     write(*,*) 'xmin and xmax are ',xmin,xmax
     write(*,*) 'ymin and ymax are ',ymin,ymax
     write(*,*) 'zmin and zmax are ',zmin,zmax
     ! AD End of value check
     
     write(*,'(a23,i3,a34,i10)') 'Finished reading level ',nlevel
     write(*,'(a8,i10,a25)') 'Keeping ',nump_level(1+nlevel),' dark matter particles...'
     if (fbar.gt.0) write(*,'(a8,i10,a17)') 'Keeping ',ngas,' gas particles...'
     write(*,*)
    end if !! End of phase == 0 print statements

    ! AD explicitly count gas particles
    ncount=ncount+nump_level(1+nlevel)+ngas
    write(*,'(a26,i12)') 'Total number of particles ',ncount
    write(*,*)

    isdmonly=.true.
  end do
 !! End loop over nlevel

  ! Write particles out to GADGET
  if (phase.eq.0) then
  write(*,'(a40)') '========================================'
  write(*,'(a32,a)') 'Writing GADGET format output to ',trim(outfile)  
  write(*,'(a40)') '========================================'
  end if

  flag=0
  
  do n=1,6
     np(n)=0
     nall(n)=0
     massarr(n)=0.0
     highword(n)=0
  end do

  if(ngas.gt.0) then
     np(1)=ngas
     massarr(1)=fbar*mpart_level(1+nlevel_max)*rescube(1+nlevel_max)
  end if
  
  np(2)=nump_level(1+nlevel_max)

  if(ngas.eq.0) fbar=0.0

  massarr(2)=(1.-fbar)*mpart_level(1+nlevel_max)*rescube(1+nlevel_max)

  i=3
  nlevel=nlevel_max

  do 
     if(i.gt.6 .or. nlevel.eq.0) exit
     np(i)=nump_level(nlevel)
     massarr(i)=mpart_level(nlevel)*rescube(nlevel)
     i=i+1
     nlevel=nlevel-1
  end do

  npart=0
  do n=1,6
     nall(n)=np(n)
     npart=npart+np(n)
     np(n)=nall(n)/NumFiles
     np_offset(n) = 0
  end do

!! Create the subfiles here, use position='append' to place onto the end of the existing file
  do subfile=1,NumFiles
    write(*,*) 'Starting subfile '
    if (NumFiles.gt.1) then
      do n=1,6
        np(n)=nall(n)/NumFiles !! Approx number of particles per file. For the last file just go to end.
        if (subfile.eq.NumFiles) np(n) = nall(n) - np_offset(n)
      end do
    end if
!! Use the following trim string and append the subfile number
    if(subfile.le.10) then
        write(unit=char_nfile, fmt='(i1)')subfile-1
    else
        if(subfile.le.100) then
            write(unit=char_nfile, fmt='(i2)')subfile-1
        else
            if(subfile.le.1000) then
                write(unit=char_nfile, fmt='(i3)')subfile-1
            else
                write(unit=char_nfile, fmt='(i4)')subfile-1
            end if
        end if
    end if
    write(*,*) 'Numer used is char_nfile ',char_nfile

    if(usehdf.eqv..true.) then
        if (NumFiles.eq.1) then
            fullfile = trim(outfile(1:strfrom))//'hdf5'
        else
            fullfile = trim(outfile(1:strfrom))//trim(char_nfile)//'.hdf5'
        end if
    else
        if (NumFiles.eq.1) then
            fullfile = trim(outfile(1:strfrom))//'bin'
        else
            fullfile = trim(outfile(1:strfrom))//trim(char_nfile)//'.bin'
        end if
    end if

!
! Create a new file using default properties.
!
    if(usehdf.eqv..true.) then

        inquire(file=fullfile, exist=fexist)
        if (fexist) then
            write(*,*) "Open ",fullfile
            CALL hdf5_open_file(file_id, fullfile)
        else
            write(*,*) "Create ",fullfile
            CALL hdf5_create_file(file_id, fullfile)
        end if
    else

        inquire(file=fullfile, exist=fexist)
        if (fexist) then
            open(1,file=fullfile,status='old',form='unformatted',action='write',position='append')
        else
            open(1,file=fullfile,status='new',form='unformatted',action='write')
        end if
    end if
!! File has been created or open ready to be appended

  if (phase.eq.0) then

!
! Create the Header group
!

  print *, massarr(2)*np(2),massarr(3)*np(3),fbar,Omega0*rhocrit*BoxSize**3
1111 format (a3,i1,a2,i11,a7,i1,a2,i11,a10,i1,a3,e18.8)
  write(*,*)
  write(*,'(a18)') 'Header Information'
  write(*,*)
  do n=1,6
     write(*,1111) 'np(',n,'):',np(n),', nall(',n,'):',nall(n),', massarr(',n,'): ',massarr(n)
  end do
  write(*,*)

1112 format (a8,f10.6,a14,f10.6,a19,f10.6)
  write(*,1112) 'Omega0: ',Omega0,', OmegaLambda:',OmegaLambda,', Hubble Parameter:',HubbleParam
1113 format (a17,f10.4,a12,f10.6,a19,f10.6)
  write(*,1113) 'BoxSize [Mpc/h]: ',BoxSize,', Redshift: ',redshift,', Expansion Factor: ',expansion
  write(*,*)
    !! Only this loop once, output positions and then velocities

    if(usehdf.eqv..true.) then
    CALL hdf5_create_group(file_id, 'Header')
    CALL hdf5_write_attribute(file_id,"/Header/NumPart_ThisFile",np)
    CALL hdf5_write_attribute(file_id,"/Header/NumPart_Total",nall)
    CALL hdf5_write_attribute(file_id,"/Header/NumPart_Total_HighWord",highword)
    CALL hdf5_write_attribute(file_id,"/Header/MassTable",massarr)
    CALL hdf5_write_attribute(file_id,"/Header/ExpansionFactor",expansion)
    CALL hdf5_write_attribute(file_id,"/Header/BoxSize",BoxSize)
    CALL hdf5_write_attribute(file_id,"/Header/NumFilesPerSnapshot",NumFiles)

    else
!! Write to binary, unformated - you get an 8 byte block 
!! separater not a 4 byte, if the particle number is 2x512^3 
!! and greater which breaks Gadget

        write(1) np,massarr,expansion,redshift,flag,flag&
            &        ,nall,flag,NumFiles,BoxSize,Omega0,OmegaLambda&
            &        ,HubbleParam,flag,flag,highword,unused
    end if
    write(*,'(a17)') 'Written header...'


!
! After the Header comes the Particles
!


!
! Create the Position group
!


  indx_offset = 1
  do n=1,6
    if(np(n).gt.0) then
        start_indx = indx_offset+np_offset(n)
        end_indx = indx_offset+np_offset(n)+np(n)-1
        write(*,*) 'n is ',n,' in subfile ',subfile, ' in pos'
        write(*,*) 'start_indx ', start_indx, ' and end_indx ',end_indx
        
        if(usehdf.eqv..true.) then
            write(partgrp,'(a8,i1)') 'PartType',n-1
            write(*,*)'Write Pos PartType',n-1

            CALL hdf5_create_group(file_id,partgrp)
!
! Create the position dataset
!
            write(partgrp,'(a8,i1,a12)') 'PartType',n-1,'/Coordinates'
            CALL hdf5_write_data(file_id,partgrp,pos(1:3,start_indx:end_indx))

        else
            write(1) ((pos(i,j),i=1,3),j=start_indx,end_indx)
        end if

       indx_offset=indx_offset+nall(n)
    end if !! Only write if particles present
  end do !! Loop over particle species

  write(*,'(a20)') 'Written positions...'
  if (subfile.eq.NumFiles) then
    deallocate(pos, STAT = AllocateStatus)
    IF (AllocateStatus /= 0) STOP "*** Couldn't deallocate pos ***"
  end if
  end if !! end of phase == 0, the positions

  if (phase.eq.1) then !! The velocities

!
! Create the Velocity group
!

  indx_offset = 1
  do n=1,6
    if(np(n).gt.0) then
        start_indx = indx_offset+np_offset(n)
        end_indx = indx_offset+np_offset(n)+np(n)-1
        write(*,*) 'n is ',n,' in subfile ',subfile, ' in vel'
        write(*,*) 'start_indx ', start_indx, ' and end_indx ',end_indx

        if(usehdf.eqv..true.) then
        write(*,*)'Write Vel PartType',n-1

!
! Create the Velocity dataset
!
        write(partgrp,'(a8,i1,a9)') 'PartType',n-1,'/Velocity'
        CALL hdf5_write_data(file_id,partgrp,vel(1:3,start_indx:end_indx))

        else
            write(1) ((vel(i,j),i=1,3),j=start_indx,end_indx)
        end if

    indx_offset=indx_offset+nall(n)
    end if !! Only write if particles present
end do !! Loop over particle species
write(*,'(a20)') 'Written velocities...'
if (subfile.eq.NumFiles) then
    deallocate(vel, STAT = AllocateStatus)
    IF (AllocateStatus /= 0) STOP "*** Couldn't deallocate vel ***"
end if

  if (npart.ge.huge(i_s)) then
     write(*,*) 'Long int IDs '
     indx_offset = 1
     do n=1,6
       if(np(n).gt.0) then
        start_indx = indx_offset+np_offset(n)
        end_indx = indx_offset+np_offset(n)+np(n)-1
        if(usehdf.eqv..true.) then

!
! Create the ID dataset
!
        allocate(longid(np(n)), STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "*** Not enough memory for longid ***"
        do i=1, np(n)
            longid(i) = start_indx + (i-1)
        end do
        write(partgrp,'(a8,i1,a12)') 'PartType',n-1,'/ParticleIDs'
        CALL hdf5_write_data(file_id,partgrp,longid)

        deallocate(longid, STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "*** Couldn't deallocate longid ***"

        else
            ! Do a binary operation
            write(1) (i,i=start_indx,end_indx)
        end if
        indx_offset=indx_offset+nall(n)

        end if !! Only write if particles present
        end do !! Loop over particle species

  else
        write(*,*) 'Short int IDs '
        indx_offset = 1
        do n=1,6
            if(np(n).gt.0) then
            start_indx = indx_offset+np_offset(n)
            end_indx = indx_offset+np_offset(n)+np(n)-1
            if(usehdf.eqv..true.) then

!
! Create the ID dataset
!
            allocate(shortid(np(n)), STAT = AllocateStatus)
            IF (AllocateStatus /= 0) STOP "*** Not enough memory for shortid ***"
            do i=1, np(n)
                shortid(i) = start_indx + (i-1)
            end do
            write(partgrp,'(a8,i1,a12)') 'PartType',n-1,'/ParticleIDs'
            CALL hdf5_write_data(file_id,partgrp,shortid)

            deallocate(shortid, STAT = AllocateStatus)
            IF (AllocateStatus /= 0) STOP "*** Couldn't deallocate shortid ***"

            else
! Do a binary operation
                write(1) (i,i=start_indx,end_indx)
            end if
            indx_offset=indx_offset+nall(n)

        end if !! Only write if particles present
        end do !! Loop over particle species
  end if
  write(*,'(a23)') 'Written particles IDs...'
  n=1
  if(np(n).gt.0) then
     if(usehdf.eqv..true.) then

!
! Create the utherm dataset
!
        allocate(uthermarr(np(1)), STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "*** Not enough memory for uthermarr ***"
        do i_s=1, np(n)
            uthermarr(i_s) = utherm
        end do
        write(partgrp,'(a8,i1,a15)') 'PartType',n-1,'/InternalEnergy'

        CALL hdf5_write_data(file_id,partgrp,uthermarr)

        deallocate(uthermarr, STAT = AllocateStatus)
        IF (AllocateStatus /= 0) STOP "*** Couldn't deallocate uthermarr ***"

    else
        write(1) (utherm,i=1,np(1))
     end if
     write(*,'(a28)') 'Written internal energies...'
  end if !! Only if gas present 

  endif !! End of phase eq 1

if(usehdf.eqv..true.) then
!
! Close the file.
!
    CALL hdf5_close_file(file_id)
else
    close(1)
end if
do n=1,6
    if (NumFiles.gt.1) np_offset(n)=np_offset(n)+np(n)
end do
write(*,*) 'At loop ',subfile,' phase ',phase,' before end subloop'
end do !! End of subfile loop
write(*,*) 'At loop ',subfile,' phase ',phase,' before end subphase'
end do !! End of loop over phase

write(*,*)
write(*,'(a17,a)') 'Finished writing ',trim(fullfile)

if(nlevel_max.gt.0) then 
    deallocate(keeparr, STAT = AllocateStatus)
    IF (AllocateStatus /= 0) STOP "*** Couldn't deallocate keeparr ***"
end if

end program lgrfc2gdt_hdf5


  
